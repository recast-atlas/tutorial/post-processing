# Post Processing
This is meant to be that repo that is the set of tools you use
for making plots and scaling stuff before you do your fitting.  It's very limited in
this simple example.

## Usage
The script can be used as
```
python scale_to_lumi_xsec.py -i selected.root -o selected_scaled.root -p scaled.pdf -c 1 -s 1 -k 1 -f 1 -l 1 -g mjj
```
the arguments are :
  - `-i`: The input file containing the histogram you want to scale
  - `-o`: The output file where you will write the scaled histogram
  - `-p`: The filename for making the debug plot
  - `-c`: cross section
  - `-s`: The sum of event weights
  - `-k`: k-factor
  - `-f`: filter efficiency
  - `-l`: luminosity
  - `-g`: The histogram in the file that is to be scaled
