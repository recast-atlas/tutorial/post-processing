FROM atlas/analysisbase:21.2.85-centos7
COPY . /code
WORKDIR /code
RUN sudo chown -R atlas /code

# Add atlas user to root group
RUN source ~/release_setup.sh && sudo usermod -aG root atlas
