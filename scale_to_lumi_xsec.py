import ROOT
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputfile", type=str, help="")
    parser.add_argument("-o", "--outputfile", type=str, help="")
    parser.add_argument("-g", "--graph", type=str, help="")
    parser.add_argument("-p", "--plotfile", type=str, help="")
    parser.add_argument("-c", "--xsection", type=str, help="")
    parser.add_argument("-s", "--sumofweights", type=str, help="")
    parser.add_argument("-k", "--kfactor", type=str, help="")
    parser.add_argument("-f", "--filterfactor", type=str, help="")
    parser.add_argument("-l", "--luminosity", type=str, help="")

    args = parser.parse_args()
    inputfile = args.inputfile
    outputfile = args.outputfile
    graph = args.graph
    plotfile = args.plotfile
    xsection = args.xsection
    sumofweights = args.sumofweights
    kfactor = args.kfactor
    filterfactor = args.filterfactor
    luminosity = args.luminosity

    print("inputfile    : ", inputfile)
    print("outputfile   : ", outputfile)
    print("graph        : ", graph)
    print("plotfile     : ", plotfile)
    print("xsection     : ", xsection)
    print("sumofweights : ", sumofweights)
    print("kfactor      : ", kfactor)
    print("filterfactor : ", filterfactor)
    print("luminosity   : ", luminosity)

    # Calculate the overall scaling constant for the signal histogram
    scaling = (
        float(xsection)
        * float(filterfactor)
        * float(kfactor)
        * float(luminosity)
        / float(sumofweights)
    )

    # Collect the signal content and edges from the input file
    f_input = ROOT.TFile(inputfile)
    f_input.ls()

    h = f_input.Get(graph)

    h_scaled = h.Clone("h_scaled")
    h_scaled.Scale(scaling)

    f_output = ROOT.TFile(outputfile, "RECREATE")
    h_scaled.Write(graph)
    f_output.Close()


if __name__ == "__main__":
    main()
